import time

import  cv2.cv2 as cv
import math

path='test.jpg'

img=cv.imread(path)
pointsList=[]




def mousePoint(event,x,y,flags,params):
    if event==cv.EVENT_LBUTTONDOWN:
        size=len(pointsList)
        cv.circle(img,(x,y),5,(0,0,255),cv.FILLED)
        pointsList.append([x,y])
        print(pointsList)

def gradient(pt1,pt2):
    return (pt2[1]-pt1[1])/(pt2[0]-pt1[0])
def getAngle(pointslist):
    pt1,pt2,pt3=pointslist[-3:]
    m1=gradient(pt1,pt2)
    m2=gradient(pt1,pt3)
    angR=math.atan((m2-m1)/(1+(m2*m1)))
    angD=round(math.degrees(angR))
    print(angD)



while True:
    if len(pointsList)%3==0 and len(pointsList)>0:
        getAngle(pointsList)
        time.sleep(2)
        break
    cv.imshow('img', img)
    cv.setMouseCallback('img',mousePoint)
    if cv.waitKey(1) and 0xFF==ord('q'):
        pointsList=[]
        img=cv.imread(path)
